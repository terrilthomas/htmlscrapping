class CreateHtmls < ActiveRecord::Migration[5.0]
  def change
    create_table :htmls do |t|
    	t.column "header_one", :text
    	t.column "header_two", :text
    	t.column "header_three", :text
    	t.column "url" , :string
      t.timestamps
    end
  end
end
